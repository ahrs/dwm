#!/bin/sh

rsync -av --progress --links --delete /etc/portage/patches/x11-wm/dwm ./etc/portage/patches/x11-wm

cp /etc/portage/savedconfig/x11-wm/dwm-6.1-r1 ./etc/portage/savedconfig/x11-wm
