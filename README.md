# dwm

This contains my dwm config and any patches I'm using.

## Building

If you're using Arch Linux (btw) you can run `make pkgbuild`. This will build (but not install) a package in the current directory which can be manually installed as follows: `pacman -U *.pkg.tar.xz` (if there's more than one .pkg.tar.xz in the current directory you'll need to specify the one you want to install)

On Gentoo you should can run `make gentoo`. This will copy the provided config files to /etc/portage (deleting any existing files so back them up first_) and then run `USE="savedconfig" emerge -q dwm`.

For everything else, a wrapper for Arch's PKGBUILD is provided via `./build.sh` (NOTE: You will need to ensure you have all of the necessary dependencies installed). The script is run with `set -e` and `set -x` so will allow you to see what is going on and exit as soon as an error occurs.

