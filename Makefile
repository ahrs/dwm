DOCKER=docker
GIT=git
SUDO=sudo
BUILDUSER_UID=1000
BASE_IMAGE=archlinux/base
ROOT=/

.PHONY: clean

all: clean other

clean:
	$(SUDO) rm -rf pkg

other:
	./build.sh

install: all
	$(SUDO) cp -Rf --no-preserve=ownership pkg/usr /

uninstall:
	$(SUDO) rm -f /usr/bin/dwm
	$(SUDO) rm -rf /usr/share/doc/dwm
	$(SUDO) rm -rf /usr/share/licenses/dwm
	if [ -n "$$(find "/usr/share/licenses" -maxdepth 0 -type d -empty 2>/dev/null)" ]; then \
	  $(SUDO) rm -rf /usr/share/licenses; \
	fi
	$(SUDO) rm -f /usr/share/man/man1/dwm.1
	$(SUDO) rm -f /usr/share/xsessions/dwm.desktop

pkgbuild:
	if ! pacman -Si st > /dev/null 2>&1 && false; then \
	  $(SUDO) pacman -S --needed --noconfirm git ncurses; \
	  git clone https://aur.archlinux.org/st.git st;cd st; \
	  $(SUDO) rm -rf $(ROOT)/usr/share/terminfo/s/st $(ROOT)/usr/share/terminfo/s/st-256color `# Conflicts with st's provided terminfo`; \
	  makepkg -fsicC --noconfirm; \
	  cd ..;rm -rf st; \
	fi

	makepkg -fscC --noconfirm

gentoo:
	$(SUDO) rm /etc/portage/savedconfig/x11-wm/dwm-6.1-r1 || true
	$(SUDO) rm -rf /etc/portage/patches/x11-wm/dwm || true
	$(SUDO) cp -Rf --no-preserve=ownership etc /
	$(SUDO) env USE="savedconfig" emerge -q --usepkg=n --usepkg-exclude 'x11-wm/dwm' x11-wm/dwm

docker:
	$(SUDO) $(DOCKER) run -it --rm -v $(PWD):$(PWD) -w $(PWD) $(BASE_IMAGE) sh -c 'pacman -Syu --noconfirm make && make inDocker'
inDocker:
	pacman -Syu --noconfirm
	pacman -S --needed --noconfirm base base-devel sudo ncurses
	rm -rf $(ROOT)/usr/share/terminfo/s/st $(ROOT)/usr/share/terminfo/s/st-256color # Conflicts with st's provided terminfo
	useradd builduser -d $(PWD) -u $(BUILDUSER_UID) -U
	passwd -d builduser
	printf 'builduser ALL=(ALL) ALL\n' | tee -a /etc/sudoers
	if ! pacman -Si st; then \
	  $(SUDO) -u builduser bash -c 'set -e;cd /tmp;$(SUDO) pacman -S --needed --noconfirm git;git clone https://aur.archlinux.org/st.git st;cd st;makepkg -fsicC --noconfirm;cd ..;rm -rf st;'; \
	fi
	$(SUDO) -u builduser bash -c 'cd ~ && makepkg -fscC --noconfirm;'
