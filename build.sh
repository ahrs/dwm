#!/usr/bin/env bash

# PKGBUILD's are just bash so with a bit of help
# we can use it on non-arch systems as a generic
# "just built it" script
. PKGBUILD

set -e
set -x

rm -rf ./*.tar.*

wget "${source[0]}" || busybox wget "${source[0]}" || curl -O "${source[0]}"

rm -rf src pkg

mkdir src pkg

TAR="$(basename "${source[0]}")"

tar xf "$TAR" -C src || busybox tar xf "$TAR" -C src
cp "${source[1]}" src/

export srcdir="$PWD/src"
export pkgdir="$PWD/pkg"

prepare
build
package

set +x

echo 'Done!'

echo 'To install execute the following command:'
echo ''
echo '  sudo cp -Rf --no-preserve=ownership pkg/usr /'
